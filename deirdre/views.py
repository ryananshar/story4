from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def base(request):
	return render(request, 'base.html', {})

def landing(request):
	return render(request, 'landing.html', {})

def home(request):
	return render(request, 'home.html', {})

def aboutme(request):
	return render(request, 'aboutme.html', {})

def skills(request):
	return render(request, 'skills.html', {})

def gallery(request):
	return render(request, 'gallery.html', {})
