from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.base, name="base"),
    path('landing/', views.landing, name="landing"),
    path('home/', views.home, name="home"),
    path('skills/', views.skills, name="skills"),
]
